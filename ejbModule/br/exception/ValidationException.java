package br.exception;

public class ValidationException extends Exception{

	private static final long serialVersionUID = 8710967377821669084L;
	
	public ValidationException(String message) {
		super(message);
	}

}
