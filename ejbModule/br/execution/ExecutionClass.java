package br.execution;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timer;

import br.entity.City;
import br.pagination.PaginatedList;
import br.session.GenericSession;

@Stateless
public class ExecutionClass implements Serializable{

	private static final long serialVersionUID = 4777882051528115876L;
	
	@EJB
	private GenericSession<City> generic;
	
	
	/**
	 * Note it's necessary implement the persistence.xml
	 * and it's necessary create city structure. 
	 * The main point here is show how to implement a pagination
	 * using only java objects (AbstractLazyList)
	 */
	@Schedule(hour="*", minute="*", second="*/10")
	public void executePaginatedList(Timer t){
		List<City> pagination = new PaginatedList<City>(generic, new City(), "cityName");
		for(City city: pagination){
			System.out.println(city.getCityName());
		}
	}

}
