package br.session;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import br.dao.GenericDao;
import br.exception.ValidationException;
import br.order.TypeOrder;

@Stateless
@Local
public class GenericSession<T> implements Serializable{

	private static final long serialVersionUID = 3235565028980347621L;

	@EJB
	protected GenericDao<T> dao;

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public T save(T t) throws ValidationException {
		validarInsert(t);
		return dao.insert(t);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public T saveOrUpdate(T t) throws ValidationException {
		validar(t);
		return dao.insertOrUpdate(t);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public T update(T t) throws ValidationException {	
		return update(t, false);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public T update(T t, Boolean initializeDependencies)
			throws ValidationException {
		validarUpdate(t);
		return dao.update(t, initializeDependencies);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void delete(T t) throws ValidationException {
		validarDelete(t);
		dao.delete(t);
	}

	public List<T> findAll(Class<T> modelClass, Boolean initializeDependecies){
		return dao.findAll(modelClass, initializeDependecies);
	}
	
	public List<T> findAll(Class<T> modelClass, Boolean initializeDependecies, String orderField){
		return dao.findAll(modelClass, initializeDependecies, orderField);
	}
	
	public List<T> findAll(Class<T> modelClass, Boolean initializeDependecies, String orderField, TypeOrder ordenacao){
		return dao.findAll(modelClass, initializeDependecies, orderField, ordenacao);
	}
	
	public List<T> findAll(Class<T> modelClass, Long clinicaId){
		return dao.findAll(modelClass, false);
	}
	
	public List<T> example(T bean, Boolean initializeDependecies){
		return dao.example(bean, initializeDependecies);
	}
	
	public T findById(Class<T> modelClass, Object id){
		return dao.findById(modelClass, id, false);
	}
	
	public T findById(Class<T> modelClass, Object id, Boolean initializeDependencies){
		return dao.findById(modelClass, id, initializeDependencies);
	}
	
	@SuppressWarnings("unchecked")
	public T initializeDependecies(T t){
		return (T) dao.initializeDependecies(t);
	}

	public List<T> findAll(T modelClass, Integer startPage, Integer maxPage, String orderField){
		return dao.findAll(modelClass, startPage, maxPage, orderField, false);
	} 
	
	public List<T> findAll(T modelClass, Integer startPage, Integer maxPage, String orderField, Boolean initializeDependecies){
		return dao.findAll(modelClass, startPage, maxPage, orderField, initializeDependecies);
	}
	
	public List<T> findAll(T modelClass, Integer startPage, Integer maxPage, String orderField, Boolean initializeDependecies, TypeOrder ordenacao){
		return dao.findAll(modelClass, startPage, maxPage, orderField, initializeDependecies, ordenacao);
	}
	
	public Long count(T modelClass){
		return dao.count(modelClass);
	}

	/**
	 * Validação antes da inserção
	 * @param t
	 * @throws ValidationException
	 */
	protected void validarInsert(T t) throws ValidationException {
		validar(t);
	}
	
	public void flush(){
		dao.flush();
	}

	/**
	 * Validação antes de um update
	 * @param t
	 * @throws ValidationException
	 */
	protected void validarUpdate(T t) throws ValidationException {
		validar(t);
	}

	/**
	 * Validação antes do delete
	 * @param t
	 * @throws ValidationException
	 */
	protected void validarDelete(T t) throws ValidationException {}

	/**
	 * Esse método deve ser sobrescrito caso haja necessidade de validação server-side
	 * @param t
	 * @throws ValidationException
	 */
	protected void validar(T t) throws ValidationException {}
	
}
