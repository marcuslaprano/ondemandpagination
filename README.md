# README #

This project shows how to create an on demand pagination to your system.

I used an EJB module to demonstrate how to implement.

You will need to adapt this code to runs inside your code.

The main class is PaginatedList.java class - This is the class that do the pagination job.

Keep in mind that you will need to improve this class, to adapt this class to your own system
